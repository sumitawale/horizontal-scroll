document.addEventListener("DOMContentLoaded", function(event) {
    "use strict"
    // settingWidth();
    resizeHanlder();
});

function settingWidth() {
    let win = window;
    let elemHolder = document.querySelector('.horizontal-scroll__column-holder');
    let getImage = document.querySelectorAll('.img-thumbnail');
    const elemWidth = 600;
    const smElemWidth = 282;

    // function getTotalWidth(element, customWidth) {
    //     return (customWidth * element.length);
    // }

    //sHorthand arrow function to get total width
    getTotalWidth = (element, customWidth) => (customWidth * element.length);

    function setWidth(element, customWidth) {
        const setTotalWidth = getTotalWidth(getImage, customWidth);
        element.style.width = setTotalWidth + 'px';
    }

    function loadContent() {
        /*if(win.innerWidth < 768 ){
            setWidth(elemHolder, smElemWidth);
        }else {
            setWidth(elemHolder, elemWidth);
        }*/

        //Short hand of if else
        win.innerWidth < 768 ? setWidth(elemHolder, smElemWidth) : setWidth(elemHolder, elemWidth);
    }

    if(!elemHolder) {
        return true;
    }else {
        win.addEventListener("resize", loadContent);
        loadContent();
    }
}

function resizeHanlder() {
    let win = window;
    let getHtml = document.querySelector('html');
    const activeClass = "resize-active";

    getWindowWidth = () => win.innerWidth;

    addClass = (element) => element.classList.add(activeClass);

    removeClass = (element) => element.classList.remove(activeClass);

    win.addEventListener("resize", function () {
        const currentWindowWidth = setTimeout(function () {
            return getWindowWidth();
        },50);

        if( getWindowWidth() < currentWindowWidth || getWindowWidth() > currentWindowWidth ) {
            addClass(getHtml);
        }

        setTimeout(function () {
            removeClass(getHtml);
        },500);
    });
}